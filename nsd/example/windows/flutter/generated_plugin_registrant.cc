//
//  Generated file. Do not edit.
//

// clang-format off

#include "generated_plugin_registrant.h"

#include <nsd_windows/nsd_windows_plugin.h>

void RegisterPlugins(flutter::PluginRegistry* registry) {
  NsdWindowsPluginRegisterWithRegistrar(
      registry->GetRegistrarForPlugin("NsdWindowsPlugin"));
}
